package es.sanitas.demo.calculadora.application;

import es.sanitas.demo.calculadora.domain.service.Calculator;
import io.corp.calculator.TracerImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
public class CalculatorRest {

    @Autowired
    Calculator calc;

    @Autowired
    TracerImpl trace;


    private BigDecimal doOperation( final BigDecimal a,
                                    final String op,
                                    final BigDecimal b) {

        BigDecimal result = calc.binaryOperation(a,b,op);
        trace.trace(String.format("Resultado de la operacion %s %s %s = %s",a,op,b,result));

        return result;
    }

    @GetMapping("/holaMundo")
    public ResponseEntity<String> holaMundo() {
        trace.trace("Petión recibida a /holaMundo");
        return new ResponseEntity<>("Hola mundo!", HttpStatus.OK);
    }
    @GetMapping("/binaryOp/{a}/{op}/{b}")
    @Operation(summary = "Realiza la operacion aritmetica a op b")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Operación realizada",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = BigDecimal.class))}),
            @ApiResponse(responseCode = "400", description = "Parametros incorrectos",
                    content = @Content),
            @ApiResponse(responseCode = "501", description = "Operación no implementada",
                    content = @Content) })
    public ResponseEntity<BigDecimal> binaryOp(@Parameter(description = "Primer operador") @PathVariable final BigDecimal a,
                                               @Parameter(description = "operación") @PathVariable final String op,
                                               @Parameter(description = "Segundo operador") @PathVariable final BigDecimal b) {
        return new ResponseEntity<>(doOperation(a,op,b), HttpStatus.OK);
    }



    @GetMapping("/calculate")
    @Operation(summary = "Realiza la operacion aritmetica a op b")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Operación realizada",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = BigDecimal.class))}),
            @ApiResponse(responseCode = "400", description = "Parametros incorrectos",
                    content = @Content),
            @ApiResponse(responseCode = "501", description = "Operación no implementada",
                    content = @Content) })
    public ResponseEntity<BigDecimal> calculate(@Parameter(description = "Primer operador") @RequestParam final BigDecimal a,
                                                @Parameter(description = "Operación") @RequestParam final String op,
                                                @Parameter(description = "Segundo operador") @RequestParam final BigDecimal b) {
        return new ResponseEntity<>(doOperation(a,op,b), HttpStatus.OK);
    }

}
