package es.sanitas.demo.calculadora.exception;

import io.corp.calculator.TracerImpl;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;


@RestControllerAdvice
public class RestExceptionControler {

    TracerImpl tr = new TracerImpl();

    @ExceptionHandler(value = {NumberFormatException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorMessage resourceNotFoundException(NumberFormatException ex, WebRequest request) {
        tr.trace("Excepcion:"+ex);
        return new ErrorMessage(
                HttpStatus.BAD_REQUEST.value(),
                new Date(),
                ex.getMessage(),
                "Los operados deben tener un formato compatible con el tipo BigDecimal de java");

    }
    @ExceptionHandler(value = {CalculatorException.class})
    @ResponseStatus(value = HttpStatus.NOT_IMPLEMENTED)
    public ErrorMessage resourceNotFoundException(CalculatorException ex, WebRequest request) {
        tr.trace("Excepcion:"+ex);
        return new ErrorMessage(
                HttpStatus.NOT_IMPLEMENTED.value(),
                new Date(),
                ex.getMessage(),
                "Operacion aritmetica no soportada");

    }

}
