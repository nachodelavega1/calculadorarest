package es.sanitas.demo.calculadora.exception;

public class CalculatorException extends RuntimeException{

    public CalculatorException(String msg) {
        super(msg);
    }
}
