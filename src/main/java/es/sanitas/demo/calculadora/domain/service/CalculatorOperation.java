package es.sanitas.demo.calculadora.domain.service;

import java.math.BigDecimal;

public interface CalculatorOperation {

    BigDecimal perform(BigDecimal a, BigDecimal b);
}
