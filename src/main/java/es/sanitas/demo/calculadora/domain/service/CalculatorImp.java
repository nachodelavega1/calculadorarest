package es.sanitas.demo.calculadora.domain.service;

import es.sanitas.demo.calculadora.exception.CalculatorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class CalculatorImp implements Calculator{

    @Autowired
    private ApplicationContext appContext;
    @Override
    public BigDecimal binaryOperation(BigDecimal a, BigDecimal b, String operator) throws CalculatorException {
        CalculatorOperation op=null;

        try {
            op = appContext.getBean(operator, CalculatorOperation.class);
        } catch (Exception e) {
            throw new CalculatorException("Operacion no implementada");
        }
        return op.perform(a,b);
    }
}
