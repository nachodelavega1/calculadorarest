package es.sanitas.demo.calculadora.domain.service;

import es.sanitas.demo.calculadora.exception.CalculatorException;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
@Component
public interface Calculator {

    BigDecimal binaryOperation(BigDecimal a, BigDecimal b, String operator) throws CalculatorException;

}
