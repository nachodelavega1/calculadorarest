package es.sanitas.demo.calculadora.domain.service;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;
@Component("-")
public class Subtraction implements CalculatorOperation {


    @Override
    public BigDecimal perform(BigDecimal a, BigDecimal b) {
        return a.subtract(b);
    }
}
