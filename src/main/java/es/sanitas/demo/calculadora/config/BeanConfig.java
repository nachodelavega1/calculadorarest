package es.sanitas.demo.calculadora.config;

import es.sanitas.demo.calculadora.domain.service.CalculatorImp;
import io.corp.calculator.TracerImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = CalculatorImp.class)
public class BeanConfig {

    @Bean
    public TracerImpl getTracerImpl() {
        return new TracerImpl();
    }
}
