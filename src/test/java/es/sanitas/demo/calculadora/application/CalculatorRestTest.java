package es.sanitas.demo.calculadora.application;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CalculatorRestTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void holaMundo() throws Exception {
        this.mockMvc.perform(get("/holaMundo")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("Hola mundo")));
    }
    @Test
    void sumaQP() throws Exception {
        this.mockMvc.perform(get("/calculate?a=2&op=+&b=2")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string("4"));
    }
    @Test
    void restaQP() throws Exception {
        this.mockMvc.perform(get("/calculate?a=2&op=-&b=2")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string("0"));
    }
    @Test
    void multiplicacionQP() throws Exception {
        this.mockMvc.perform(get("/calculate?a=2&op=*&b=2")).andDo(print()).andExpect(status().is(501));
    }
    @Test
    void operadoresIncorrectosQP() throws Exception {
        this.mockMvc.perform(get("/calculate?a=a&op=%2B&b=b")).andDo(print()).andExpect(status().is(400));
    }

    @Test
    void suma() throws Exception {
        this.mockMvc.perform(get("/binaryOp/2/+/2")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string("4"));
    }
    @Test
    void resta() throws Exception {
        this.mockMvc.perform(get("/binaryOp/2/-/2")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string("0"));
    }
    @Test
    void multiplicacion() throws Exception {
        this.mockMvc.perform(get("/binaryOp/2/*/2")).andDo(print()).andExpect(status().is(501));
    }
    @Test
    void operadoresIncorrectos() throws Exception {
        this.mockMvc.perform(get("/binaryOp/a/*/b")).andDo(print()).andExpect(status().is(400));
    }


}
