package es.sanitas.demo.calculadora.exception;

import org.junit.jupiter.api.Test;

import java.sql.Timestamp;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ErrorMessageTest {

    private String strVal="valor";
    private int intVal=9;
    private Timestamp tmVal = new Timestamp(0);
    private ErrorMessage err = new ErrorMessage(intVal,tmVal,"message","description");


    @Test
    void testConstructor() {
        assertEquals(intVal,err.getStatusCode());
        assertEquals(tmVal,err.getTimestamp());
        assertEquals("message",err.getMessage());
        assertEquals("description",err.getDescription());
    }
    @Test
    void testGetterSetter() {
        err.setStatusCode(intVal);
        assertEquals(intVal,err.getStatusCode());
        err.setTimestamp(tmVal);
        assertEquals(tmVal,err.getTimestamp());
        err.setMessage(strVal);
        assertEquals(strVal,err.getMessage());
        err.setDescription(strVal);
        assertEquals(strVal,err.getDescription());
    }

}
