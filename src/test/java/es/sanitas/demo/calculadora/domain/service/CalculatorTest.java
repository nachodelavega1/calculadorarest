package es.sanitas.demo.calculadora.domain.service;

import es.sanitas.demo.calculadora.exception.CalculatorException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;


import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
@ExtendWith(SpringExtension.class)
public class CalculatorTest {

    //private final Calculator cal = new CalculatorImp();


    @Autowired
    private ApplicationContext appContext;

    @Test
    void suma() {
        BigDecimal a = new BigDecimal(1);
        BigDecimal b = new BigDecimal(1);
        BigDecimal esperado = new BigDecimal(2);
        Calculator cal= appContext.getBean(CalculatorImp.class);
        assert (cal.binaryOperation(a,b,"+").equals(esperado));
    }

    @Test
    void resta() {
        BigDecimal a = new BigDecimal(1);
        BigDecimal b = new BigDecimal(1);
        BigDecimal esperado = new BigDecimal(0);
        Calculator cal= appContext.getBean(CalculatorImp.class);
        assert (cal.binaryOperation(a,b,"-").equals(esperado));
    }
    @Test
    void multiplicar() {
        BigDecimal a = new BigDecimal(1);
        BigDecimal b = new BigDecimal(1);
        Calculator cal = appContext.getBean(CalculatorImp.class);
        Exception exception = assertThrows (CalculatorException.class, () -> cal.binaryOperation( a,b,"*"));

        assertEquals("Operacion no implementada", exception.getMessage());

    }

}
