package es.sanitas.demo.calculadora;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

@SpringBootTest
class CalculadoraApplicationTests {

	@Test
	void contextLoads(ApplicationContext context) {
		CalculadoraApplication.main(new String[] {"test1", "test2"});
		assertNotEquals(context, null);
	}

}
