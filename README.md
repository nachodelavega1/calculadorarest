# Prueba Técnica – Desarrollo de un Microservicio Calculadora 

## Estructura del proyecto

Estructura estandar de un proyecto en java utilizando maven, con el directorio de fuentes y el equivalente de pruebas unitarias. Esta estructura base se ha creado descomprimiendo el fichero generado desde Spring Initializr. Los packages definidos para las clases se basan en una aproximación a lo que seria un proyecto usando arquitectura hexagonal, separando el dominio (la calculadora), la aplicacion (controlador REST). En este caso no hay infraestructura.

Hay un directorio raiz, /lib, en el que se ubicó la libreria tracer.jar, para poder gestionarla como dependencia en el pom.xml de la aplicación 


## Dependencias del proyecto

La ejecución del proyecto requiere tener instalado JDK 19 y maven compatible con este jdk.
Las dependencias de liberias, configuradas en el pom.xml son:
- org.springframework.boot.spring-boot-starter-web (3.0.2)
- org.springframework.bootspring-boot-starter-test (3.0.2)
- io.corp.calculator.tracer (1.0.0)
- org.springdoc.springdoc-openapi-starter-webmvc-ui (2.0.2)

## Funcionalidades implementadas

Se han definido dos endpoint http de tipo GET para realizar operaciones aritmeticas que impliquen 2 operadores, /binaryOP y /calculate. La única diferencia entre ambos es que /calculate recibe los datos mediante parametros en el queryString y /binaryOp mediante la url del endpoint. 

Los operadores deben ser convertibles al formato admitido para el tipo de dato [BigDecimal](https://docs.oracle.com/en/java/javase/19/docs/api/java.base/java/math/BigDecimal.html#<init>(java.lang.String)) de java. 

La realización de la operación es delegada al objeto del dominio CalculatorImp, que implementa el interface Calculator. En esta versión este objeto es capaz de realizar sumas y restas. El código está desarrollado de tal manera que permite añadir nuevas operaciones aritmeticas sin modificar el código existente. Para ello es necesario crear nuevas clases que implementen el interfaz CalculatorOperation. Estas nuevas clases deben ir anotadas como @Component("Operador"), P.E. @Component("*"). La clase CalculatorImp instancia el objeto adecuado para hacer la operación solicitada mediante el uso de ApplicationContext.getBean(operator, CalculatorOperation.class);

Se han gestionado las excepciones lanzadas desde el controlador REST mediante una clase anotada con @RestControllerAdvice

Se han desarrollado pruebas unitarias para las clases desarrolladas, alacanzando una cobertura del 100% de las lineas de codigo programadas.

## Compilación y ejecución

``` 
git clone https://gitlab.com/nachodelavega1/calculadorarest.git
cd calculadorarest/
mvn validate
mvn install
java -jar target/calculadora-0.0.1-SNAPSHOT.jar
curl 127.0.0.1:8080/binaryOp/2/+/2
```

## Documentación 

La documentación de los endpoints del servicio REST está disponible en formato Swagger en http://localhost:8080/swagger-ui/index.html
 